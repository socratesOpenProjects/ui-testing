﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class CreditHolders : MonoBehaviour
{
    //CreditHolders script is attached to the Currency Panel prefab, and updates the banance after being notified by
    //an event coming from the CreditManager script.


    [SerializeField] TMP_Text creditText;  //the text that holds the credit value
    CreditsManager creditsManager;  //a reference to the CreditManager in order to subscribe to the OnBalanceChanged event
    void Start()
    {
        //Getting a reference of the CreditsManager & getting the currentBalance
        creditsManager = FindObjectOfType<CreditsManager>();
        creditText.text = creditsManager.GetBalance().ToString();

        //Subscribing to the OnBalanceChanged event
        creditsManager.OnBalanceChanged += CreditsManager_OnBalanceChanged;
    }

    //When the event arrives we access the creditsManager to update the balance
    private void CreditsManager_OnBalanceChanged(object sender, System.EventArgs e)
    {
        creditText.text = creditsManager.GetBalance().ToString();
    }
}
