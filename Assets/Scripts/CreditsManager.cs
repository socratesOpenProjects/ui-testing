﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;

public class CreditsManager : MonoBehaviour
{
    //The CreditsManager script handles and communicates the credits value

    [SerializeField] int credits = 1000;

    //Creating an event to call subscibers to update the balance value
    public event EventHandler OnBalanceChanged;

    private void Awake()
    {
        //initializing credits value
        credits = 1000;
    }

    //Returns the current credit balance
    public int GetBalance()
    {
        return Mathf.Clamp(credits,0,credits);
    }

    //update the credit balance
    public void UpdateBalance(int value)
    {
        credits += value;

        //use this method to trigger the event
        TriggerOnBalanceChangedEvent();   
    }

    private void TriggerOnBalanceChangedEvent()
    {
        //make sure that the OnBalanceChanged event has subscribers before being transmitted to subscribers
        if (OnBalanceChanged != null)
        {
            OnBalanceChanged(this, EventArgs.Empty);

        }
    }
}
