﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class LevelUpManager : MonoBehaviour
{
    //The LevelUpManager handles everything related to player Leveling up. 

    [Header("Level Up Settings")]
    [SerializeField] ParticleSystem playerLevelUpVFX;
    [SerializeField] GameObject player;
    [SerializeField] int levelUpRequirement = 200;
    [SerializeField] int playerLevel = 1;
    [SerializeField] int playerPower = 1560;
    [SerializeField] float playerPowerLevelUpModifier = 1.05f;

    [Header("UI Elements")]
    [SerializeField] TMP_Text playerLevelText;
    [SerializeField] Button levelUpButton;
    [SerializeField] TMP_Text playerPowerText;
    

    [SerializeField] CreditsManager creditsManager;

    
    private void Awake()
    {
        //find creditsManager in the scene
        creditsManager = FindObjectOfType<CreditsManager>();

        //initialize the Level and Power text field when the game begins
        playerLevelText.text = playerLevel.ToString();
        playerPowerText.text = playerPower.ToString();
    }
    //The method called by the LevelUp button
    public void LevelUp()
    {
        //if there are the proper conditions for the player to level proceed
        if(CheckIfLevelUpPossible())
        {
            PlayAnimation();
            ParticlesEmission();
            UpdatePlayerLevel();
            GainPower();
            creditsManager.UpdateBalance(-levelUpRequirement);
            UpdateLevelUpButtonStatus();
        }       
    }

    //abstact mechanic created to visualize player's power increase during level up
    private void GainPower()
    {
        playerPower =+ Mathf.RoundToInt((playerPower * playerPowerLevelUpModifier));
        playerPowerText.text = playerPower.ToString();
    }

    //update the player's level in the UI
    private void UpdatePlayerLevel()
    {
        playerLevel++;
        playerLevelText.text = playerLevel.ToString();
    }

    //emit the particles related to level up
    private void ParticlesEmission()
    {
        playerLevelUpVFX.Play();
    }

    //trigger the "level up" trigger parameter to transit from the "idle" to the "level up" animation
    private void PlayAnimation()
    {
        player.GetComponent<Animator>().SetTrigger("level up");
    }

    //quick bool check if the conditions for leveling up are met 
    private bool CheckIfLevelUpPossible()
    {
        return (creditsManager.GetBalance() >= levelUpRequirement);
    }

    //update the appearance of the LevelUpButton. If the levelUp is not possible the button is not interactable. Otherwise the 
    //button is tinted green. 
    public void UpdateLevelUpButtonStatus()
    {
        if(!CheckIfLevelUpPossible())
        {
            levelUpButton.interactable = false;
        }
        else
        {
            levelUpButton.interactable = true;
        }
    }
}
