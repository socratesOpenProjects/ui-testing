﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using System;

public class MenuManager : MonoBehaviour
{
    //The menuManager is being used to control the behaviour of the Menu, in terms of UI and settings. It dictates the way the 
    //navButtons function and passes infomation to other components such as the PageSwipper script, of the Locker Room page. 

    /* Last minute note: obviously the MenuManager has become more expanded than should it should be and therefore should be
     * split into different subManagers in order for the code to be more clear*/

    [SerializeField] Button[] navButtons;
    [SerializeField] RectTransform[] menuPages;
    [SerializeField] RectTransform panelHolder;
    [SerializeField] PageSwiper pageSwiper;

    [Header("Nav Buttons Animation Settings")]
    [SerializeField] float shakePower = 50f;
    [SerializeField] float shakeDuration = 0.5f;

    [Header("Locker Room Settings")]
    [SerializeField] RectTransform playerDetailsScreen;
    PlayerSpriteSpawner playerSpawner;
    bool playerDetailsScreenActivated = false;

    [Header("3d player Settings")]
    [SerializeField] GameObject player;

    [Header("CreditsManager Settings")]
    [SerializeField] CreditsManager creditsManager;

    int activeButtonIndex=3;


    private void Start()
    {
        //making sure the PanelHolder pos is set to 0 in x
        //ResetPlanelHolderPosition();

        //We land on the main menu page which has an index of 2
        UpdateButtonsColor(2);

        //find Player Spawner
        playerSpawner = FindObjectOfType<PlayerSpriteSpawner>();

        //find creditsManager
        creditsManager = FindObjectOfType<CreditsManager>();
    }
    

    //Hooked to Button1
    public void NavigateToPage1()
    {
        //Check if the PlayerDetailsPanel is activated. If yes, close it and execute the remaining code.
        CheckIfPlayerDetailsPanelIsActivated();

        //this is a simple value so the we know what page/navButton is active
        activeButtonIndex = 0;

        //Animate, update the color and transit the PanelHolder to a new Location
        AnimateButton(activeButtonIndex);
        UpdateButtonsColor(activeButtonIndex);
        Vector2 newLocation = new Vector2(menuPages[0].anchoredPosition.x, panelHolder.anchoredPosition.y);
        MovePanelToLocation(newLocation);
    }

    //Hooked to Button1. The functionality is the same as Button1
    public void NavigateToPage2()
    {
        CheckIfPlayerDetailsPanelIsActivated();
        activeButtonIndex = 1;
        AnimateButton(activeButtonIndex);        
        UpdateButtonsColor(activeButtonIndex);
        Vector2 newLocation = new Vector2(menuPages[1].anchoredPosition.x, panelHolder.anchoredPosition.y);
        MovePanelToLocation(newLocation);
    }

    //Main menu page
    public void NavigateToMainPage()
    {
        CheckIfPlayerDetailsPanelIsActivated();
        activeButtonIndex = 2;
        AnimateButton(activeButtonIndex);   
        UpdateButtonsColor(activeButtonIndex);
        Vector2 newLocation = new Vector2(menuPages[2].anchoredPosition.x, panelHolder.anchoredPosition.y);
        MovePanelToLocation(newLocation);
    }

    //Locker Room page
    public void NavigateToLockerRoomPage()
    {
        CheckIfPlayerDetailsPanelIsActivated();
        activeButtonIndex = 3;
        AnimateButton(activeButtonIndex);       
        UpdateButtonsColor(activeButtonIndex);
        Vector2 newLocation = new Vector2(menuPages[3].anchoredPosition.x, panelHolder.anchoredPosition.y);
        MovePanelToLocation(newLocation);
        CheckIfPlayersSpawned();

    }
    public void NavigateToPage5()
    {
        CheckIfPlayerDetailsPanelIsActivated();
        activeButtonIndex = 4;
        AnimateButton(activeButtonIndex);
        UpdateButtonsColor(activeButtonIndex);
        Vector2 newLocation = new Vector2(menuPages[4].anchoredPosition.x, panelHolder.anchoredPosition.y);        
        MovePanelToLocation(newLocation);
    }

    //We want to close the playerDetails page if the user presses a nav button.
    private void CheckIfPlayerDetailsPanelIsActivated()
    {
        if (playerDetailsScreenActivated)
        {
            DeAvtivatePlayerDetails();
        }
    }

    //When we arrive on the Locker Room page(page4), make a check if the icons are spawned. If not, spawn.
    private void CheckIfPlayersSpawned()
    {
        if (!playerSpawner.hasSpawned)
        {
            playerSpawner.Spawn();
        }
    }

    //After a navButton is pressed, move the PanelHolder to the newLocation
    private void MovePanelToLocation(Vector2 newLocation)
    {
        panelHolder.DOAnchorPos(newLocation, 0.15f);
        pageSwiper.SetPanelLocation(newLocation);
    }

    //highlight the active navButton's color to show it is activated
    void UpdateButtonsColor(int index)
    {      
        for (int i = 0; i < navButtons.Length; i++)
        {
            navButtons[i].GetComponent<Image>().color = new Color(1, 1, 1);
        }
        navButtons[index].GetComponent<Image>().color = new Color(0.25f, 0.5f, 0.25f); //using a green color overlay. the value can
        //be exposed to the declarations
    }

    //Animate the active nav button with DOTween. The animation is being called both by the NavButtons as well as during page swipping
    private void AnimateButton(int activeButtonIndex)
    {
        navButtons[activeButtonIndex].GetComponent<RectTransform>().DOShakeRotation(shakeDuration, shakePower);
    }
    
    //animate&colorize the navButtons when finger swipping
    public void AnimateButtonsFromSwipe(Vector3 pos)
    {
        if(pos.x == menuPages[0].anchoredPosition.x)
        {
            UpdateButtonsColor(0);
            AnimateButton(0);
        }

        else if (pos.x == menuPages[1].anchoredPosition.x)
        {
            UpdateButtonsColor(1);
            AnimateButton(1);
        }

        else if (pos.x == menuPages[2].anchoredPosition.x)
        {
            UpdateButtonsColor(2);
            AnimateButton(2);
        }

        else if (pos.x == menuPages[3].anchoredPosition.x)
        {
            UpdateButtonsColor(3);
            AnimateButton(3);
            if (!playerSpawner.hasSpawned)
            {
                playerSpawner.Spawn();
            }          
        }

        else if (pos.x == menuPages[4].anchoredPosition.x)
        {
            UpdateButtonsColor(4);
            AnimateButton(4);  
        }
    }

    //Calculating the swipe borders based on the total number of the menuPages array, and the pos of the first and last pages of the
    //array. Make sure you set the pages in the array in a serial manner(ie page1, page2,...page5).
    public Vector2 GetSwipeBorders()
    {
        int numberOfPages = menuPages.Length;
        float minBorder = menuPages[numberOfPages-1].anchoredPosition.x;
        float maxBorder = menuPages[0].anchoredPosition.x;

        return new Vector2(minBorder, maxBorder);
    }

    //Get the number of menu pages in the scene. Used by the PageSwiper script to define swipe distance
    public int GetNumberOfPages()
    {
        int numberOfPages = menuPages.Length;
        return numberOfPages;
    }

    //Reset PanelHolder Position to 0
    public Vector2 ResetPlanelHolderPosition()
    {
        float newHolderPosition = 0;
        panelHolder.anchoredPosition = new Vector2(newHolderPosition, 0);
        return panelHolder.anchoredPosition;
    }

    //brings down the PlayerDetails screen
    public void ActivatePlayerDetailsScreen()
    {
        playerDetailsScreen.DOAnchorPosY(0f, 0.5f);

        //we randomize the 3d player's color for some variety. 
        Randomize3DPlayerColor();
        playerDetailsScreenActivated = true;
    }

    //this method is being called by the "X" button the PlayerDetails page. Otherwise, it is being called by the navButtons
    public void DeAvtivatePlayerDetails()
    {
        playerDetailsScreen.DOAnchorPosY(1600f, 0.5f);
        playerDetailsScreenActivated = false;
    }

    //simple script to randomize each 3D player for visualisation reasons.
    private void Randomize3DPlayerColor()
    {
        player.GetComponent<MeshRenderer>().material.color = new Color(UnityEngine.Random.Range(0f, 1f),
                                                            UnityEngine.Random.Range(0f, 1f),
                                                            UnityEngine.Random.Range(0f, 1f));
    }





}
