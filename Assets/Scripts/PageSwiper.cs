﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class PageSwiper : MonoBehaviour, IDragHandler, IEndDragHandler
{
    //PageSwipper script controls the finger swipping performed by the user and smoothly swipes to the adjacent page
    //If we reach to the boarders(most extreme pages) the swipping is clampped. 

    [Header("Swipe Setup")]
    [SerializeField] float percentThreshold = 0.2f;
    [SerializeField] float easing = 0.25f;
    float swipeDistance;  //value of the swipe distance
    float minSwipeBorder;  //limits the swipe to the left
    float maxSwipeBorder;  //limits the swipe to the right

    private Vector3 panelLocation;  //info on the panel's location
    private MenuManager menuManager;   //reference to the menuManager

    // Start is called before the first frame update
    void Start()
    {
        //Finding and Accessing menuManager
        menuManager = FindObjectOfType<MenuManager>();
        
        //get the swipe borders, that are being calculated by the menuManager. If the menuManager is not found log an error
        if(menuManager)
        {
            //Get Info from the menuManager on the for the Swipe Limits and also Swipe Distance.
            SetSwipeBorders(menuManager.GetSwipeBorders());
            SetSwipeDistance(menuManager.GetNumberOfPages());
        }
        else
        {
            Debug.Log("<color=red>menuManager not found</color>");
        }
        
        //get the ancoredPosition of the panel
        panelLocation = GetComponent<RectTransform>().anchoredPosition;
    }

    //Set the Distance of the Swipe
    private void SetSwipeDistance(int pages)
    {
        swipeDistance = Mathf.Abs(minSwipeBorder-maxSwipeBorder)/ (pages-1);
    }

    //Set the limits after which swiping is not performed
    private void SetSwipeBorders(Vector2 borders)
    {
        minSwipeBorder = borders.x;
        maxSwipeBorder = borders.y;
    }

    //Scan for an OnDrag event and find the distance covered by the user's finger
     public void OnDrag(PointerEventData data)
    {
        float difference = data.pressPosition.x - data.position.x;
        GetComponent<RectTransform>().anchoredPosition = panelLocation - new Vector3(difference, 0, 0);
    }

    //Identify the point the finger stopped moving with an OnEndDrag event
    public void OnEndDrag(PointerEventData data)
    {
        //if the distance covered by the swipe is larger then the specified percentage threshold, move to the next page 
        float percentage = (data.pressPosition.x - data.position.x) / Screen.width;

        //using the absolute value so we don't have to do the calculation twice
        if (Mathf.Abs(percentage) >= percentThreshold)
        {
            Vector3 newLocation = panelLocation;
            
            //is the swipe left or right?
            if (percentage > 0)
            {
                newLocation += new Vector3(-swipeDistance, 0, 0);
            }
            else if (percentage < 0)
            {
                newLocation += new Vector3(swipeDistance, 0, 0);           
            }
            
            //limit the swipe based on the values set on minSwipeBorder and maxSwipeBorder
            newLocation.x = Mathf.Clamp(newLocation.x, minSwipeBorder, maxSwipeBorder);

            //use a coroutine to ease the transition and update the Panel's location
            StartCoroutine(SmoothMove(GetComponent<RectTransform>().anchoredPosition, newLocation, easing));
            panelLocation = newLocation;

            //call the menuManager to Animate the appropriate menu buttons
            menuManager.AnimateButtonsFromSwipe(newLocation);
        }
        else
        {
            //if the finger drag is not enough, return smoothly to the same page
            StartCoroutine(SmoothMove(GetComponent<RectTransform>().anchoredPosition, panelLocation, easing));           
        }

        //responsible for smoothly executing the swipe 
        IEnumerator SmoothMove(Vector3 startPos, Vector3 endPos, float seconds)
        {
            float t = 0f;
            while (t<=1.0)
            {
                t += Time.deltaTime / seconds;
                GetComponent<RectTransform>().anchoredPosition = Vector3.Lerp(startPos, endPos, Mathf.SmoothStep(0f, 1f, t));
                yield return null;
            }
        }
        
    }

    //update the panelLocation value after a button has been pressed
    public void SetPanelLocation(Vector3 location)
    {
        panelLocation = location;
    }
    
}
