﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using DG.Tweening;
using System;

public class PlayerSpriteRandomizer : MonoBehaviour, IPointerClickHandler
{
    //This class randomizes the sprites that are populated in the Locker Room Page. A single prefab is being used, and randomized by the
    //number of sprites set to the playerSprites in the PlayerSpriteSpawner class. 
    
    /*The prefab is a gameObject with a frame Image assigned on the Parent, and the player's portrait Icon on the child gameobject.
    The randomizer assigns a random character portrait to the image component of the child gameobject. Also it chooses randomly the availabe/unavailable
    players, assigning them a dark grey color. The user can click on available portraits and the player's details screen open.*/


    [SerializeField] private Image childImage;
    [SerializeField] private bool activateAvailabilityRandomization;
    [SerializeField] private bool isAvailable = true;

    private void Start()
    {
        GetComponent<RectTransform>().DOShakeScale(0.5f, 0.25f);
        RandomizeAvailability();
    }

    //Randomize the player's availability
    private void RandomizeAvailability()
    {
        if(activateAvailabilityRandomization)
        {
            int tempValue = UnityEngine.Random.Range(0, 10);
            if (tempValue > 5)
            {
                childImage.color = new Color(0.5f, 0.5f, 0.5f);
                GetComponent<Image>().color = new Color(0.5f, 0.5f, 0.5f);
                isAvailable = false;
            }
        }
    }

    //choose a random sprite
    public void RandomizeImage(Sprite image)
    {
        childImage.sprite = image;
    }

    //transit to the PlayerDetails page when clicking on an active player
    public void OnPointerClick(PointerEventData eventData)
    {
        if (isAvailable)
        {
            GetComponent<RectTransform>().DOShakeScale(0.5f, 0.25f);
            FindObjectOfType<MenuManager>().ActivatePlayerDetailsScreen();
        }
        
    }
}
