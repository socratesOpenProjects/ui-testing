﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerSpriteSpawner : MonoBehaviour
{
    //The PlayerSpriteSpawner populates the Locker Room page with the players owned by the user. 

    [SerializeField] private Transform scrollviewContent;
    [SerializeField] private GameObject prefab; //prefab to be instantiated
    [SerializeField] private List<Sprite> playerSprites; //list of available player sprites
    [SerializeField] private bool spriteListRandomization; //flag for shuffling the sprites list
    [SerializeField] public bool hasSpawned = false; //accessed by the MenuManager to determine if the players are instantiated or not


    //proceed and randomize the sprites list if true
    private void Start()
    {
        if(spriteListRandomization)
        { 
            ShuffleSpriteList();
        } 
    }

    //called by the MenuManager as soon as we access the LockerRoom page for the first time.
    public void Spawn()
    {
        StartCoroutine(SpawnPlayers());
    }

    //Shuffles sprites list for some extra visual variety when accessing the locker room. This is totally optional
    private void ShuffleSpriteList()
    {
        for (int i = 0; i < playerSprites.Count; i++)
        {
            Sprite temp = playerSprites[i];
            int randomIndex = UnityEngine.Random.Range(i, playerSprites.Count);
            playerSprites[i] = playerSprites[randomIndex];
            playerSprites[randomIndex] = temp;
        }
    }
    //This coroutine spawns the players owned by the user. It iterates through the playerSprites list under the scrollViewContent object with a delay 
    //and populates the Locker Room page
    IEnumerator SpawnPlayers()
    {
        foreach (Sprite playerPortrait in playerSprites)
        {
            yield return new WaitForSeconds(0.05f);
            GameObject newPlayer = Instantiate(prefab, scrollviewContent);

            //try to find execute the RandomizeImage method
            if (newPlayer.TryGetComponent<PlayerSpriteRandomizer>(out PlayerSpriteRandomizer player))
            {
                
                player.RandomizeImage(playerPortrait);
            }
        }
        //activating the flag that spawining is completed
        hasSpawned = true;
    }
}
